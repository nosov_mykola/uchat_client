#include "api/api.h"
#include "handlers/handlers.h"
#include "uchat.h"

void load_chat_name(chat_entry *chat) {
    gtk_label_set_text(GTK_LABEL(chat->gtk_chat_name), chat->name);
}

void load_chat_info_name(int chat_id, Store *store) {
    gtk_label_set_text(GTK_LABEL(store->chats->chats_arr[chat_id]->active_entry->chat_info_chat_name), store->chats->chats_arr[chat_id]->name);
}

void load_chat_info_bio(chat_entry *chat) {
    gtk_label_set_text(GTK_LABEL(chat->active_entry->chat_info_chat_bio), chat->description);
}

void load_chat_sec_label(int chat_id, Store *store) {
    gtk_label_set_text(GTK_LABEL(store->chats->chats_arr[chat_id]->active_entry->chat_info_sec_label), store->chats->chats_arr[chat_id]->active_entry->chat_info_sec_label_text);
}

#pragma once
#include <uchat.h>
#include "../api/api.h"
#include "../async/async.h"
#include "../chat/chats.h"
#include "settings/handlers.h"

void btn_window_close_handler(GtkWidget *widget, Store* store);
void btn_window_fullscreen_handler(GtkWidget *widget, Store* store);
void btn_window_minimize_handler(GtkWidget *widget, Store* store);
void setting_button_handler(GtkWidget *widget, Store *store);

void apply_root_handlers();

FW_PATHS=$(wildcard frameworks/*)


all: submodules $(FW_PATHS)
	$(MAKE) -C client

clean:
	$(MAKE) -C client clean

uninstall: clean
	$(MAKE) -C client uninstall

deploy: $(FW_PATHS)
	./scripts/deploy.sh

$(FW_PATHS):
	$(MAKE) -C $@

package:
	sudo apt install libgtk-3-0 libgtk-3-dev libvte-2.91-dev libmongoc-1.0-0 libmongoc-dev 

submodules: 
	git submodule init
	git submodule update

reinstall: uninstall all

.PHONY: $(FW_PATHS) deploy
